//
//  ViewController.swift
//  BRTest
//
//  Created by Omer Waqas Khan on 07/02/2020.
//  Copyright © 2020 Omer Waqas Khan. All rights reserved.
//

import UIKit
//import Eden
import testpod

class ViewController: UIViewController {
    
    @IBOutlet weak var label: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let aStr = "A String"
        
        label.text = "\(aStr) = \(aStr.count)"
        
    }
}

